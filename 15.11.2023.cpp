#include <iostream>

int sigma(int);

int main()
{
    std::cout << sigma(4);
}


int sigma(int n)
{
	if (n == 1)
	{
		return 1;
	}
    return n + sigma(n - 1);
}